{-# LANGUAGE OverloadedStrings #-}

module TaskDB
  ( TaskStatus(Done, NotDone)
  , Task
  , fetchFromDB, pushTaskToDB, completeTask
  ) where

import HTML (HTML, toHTML)

import qualified Hasql.Connection
import qualified Hasql.Session
import qualified Hasql.Statement
import qualified Hasql.Encoders
import qualified Hasql.Decoders
import qualified Data.Text
import qualified Data.ByteString.UTF8 as StrictUTF8 (fromString, toString)
import Data.Functor.Contravariant (contramap)
import GHC.Int (Int32)

data TaskStatus = Done | NotDone
  deriving (Eq, Ord, Show, Read, Enum, Bounded)

data Task = Task Int32 String TaskStatus
  deriving (Eq, Ord, Show)

instance HTML Task where
  toHTML task =
    case task of
      Task taskId description status ->
        case status of
          NotDone ->
            "<p><form action=\"/completetask\" method=\"POST\"><input name=\"taskId\" type=\"hidden\" value="
            ++ show taskId
            ++ "></input>"
            ++ description
            ++ "<input type=\"submit\" value=\"complete\"></input></form></p>"
          Done -> "<p><strike>" ++ description ++ "</strike></p>"

connectionSettings :: Hasql.Connection.Settings
connectionSettings =
  Hasql.Connection.settings
    "localhost"
    (fromInteger 5432)
    "Haskell-student"
    "Why-are-you-putting-credentials-in-code?-You-absolute-potato!"
    "todolists"

runSessionAndClose :: Show a => Hasql.Session.Session a -> IO a
runSessionAndClose session =
  do
    connectionResult <- Hasql.Connection.acquire connectionSettings
    case connectionResult of
      Left (Just errMsg) -> error $ StrictUTF8.toString errMsg
      Left Nothing -> error "Unspecified connection error"
      Right connection -> do
        sessionResult <- Hasql.Session.run session connection
        print sessionResult
        Hasql.Connection.release connection
        case sessionResult of
          Right result -> return result
          Left err -> error $ show err

fetchFromDB :: IO [Task]
fetchFromDB = runSessionAndClose selectTasksSession

selectTasksSession :: Hasql.Session.Session [Task]
selectTasksSession = Hasql.Session.statement () selectTasksStatement

selectTasksStatement :: Hasql.Statement.Statement () [Task]
selectTasksStatement =
  Hasql.Statement.Statement
    "SELECT * FROM todolist_ch8 ORDER BY id;"
    Hasql.Encoders.unit
    tasksDecoder
    True

tasksDecoder :: Hasql.Decoders.Result [Task]
tasksDecoder = Hasql.Decoders.rowList taskDecoder

boolToTaskStatus :: Bool -> TaskStatus
boolToTaskStatus True = Done
boolToTaskStatus False = NotDone

taskStatusDecoder :: Hasql.Decoders.Value TaskStatus
taskStatusDecoder = fmap boolToTaskStatus Hasql.Decoders.bool

stringDecoder :: Hasql.Decoders.Value String
stringDecoder = fmap Data.Text.unpack Hasql.Decoders.text

taskDecoder :: Hasql.Decoders.Row Task
taskDecoder = do
  taskId <- Hasql.Decoders.column Hasql.Decoders.int4
  taskDescription <- Hasql.Decoders.column stringDecoder
  taskStatus <- Hasql.Decoders.column taskStatusDecoder
  return $ Task taskId taskDescription taskStatus

pushTaskToDB :: String -> IO ()
pushTaskToDB taskDescription =
  let
    session :: Hasql.Session.Session ()
    session = Hasql.Session.statement taskDescription pushTaskStatement
  in
    runSessionAndClose session

pushTaskStatement :: Hasql.Statement.Statement String ()
pushTaskStatement =
  Hasql.Statement.Statement
    "INSERT INTO todolist_ch8 ( \
    \  task, \
    \  done \
    \)\
    \VALUES \
    \  ( $1, FALSE );"
    taskNameEncoder
    Hasql.Decoders.unit
    True

taskNameEncoder :: Hasql.Encoders.Params String
taskNameEncoder = Hasql.Encoders.param taskNameEncoder_value

taskNameEncoder_value :: Hasql.Encoders.Value String
taskNameEncoder_value = contramap Data.Text.pack Hasql.Encoders.text

completeTask :: Int32 -> IO ()
completeTask taskId =
  let
    session :: Hasql.Session.Session ()
    session = Hasql.Session.statement taskId completeTaskStatement
  in
    runSessionAndClose session

completeTaskStatement :: Hasql.Statement.Statement Int32 ()
completeTaskStatement =
  Hasql.Statement.Statement
    "UPDATE todolist_ch8 \
    \SET done=TRUE \
    \WHERE id=$1;"
    ( Hasql.Encoders.param Hasql.Encoders.int4 )
    Hasql.Decoders.unit
    True
